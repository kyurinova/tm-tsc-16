package ru.tsc.kyurinova.tm.service;

import ru.tsc.kyurinova.tm.api.repository.ITaskRepository;
import ru.tsc.kyurinova.tm.api.service.ITaskService;
import ru.tsc.kyurinova.tm.enumerated.Status;

import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task("", "", new Date());
        task.setName(name);
        taskRepository.add(task);

    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = new Task("", "", new Date());
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return Collections.emptyList();
        return taskRepository.findAll(comparator);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(id);
    }

    @Override
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(name);
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(name);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.startById(id);
    }

    @Override
    public Task startByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.startByIndex(index);
    }

    @Override
    public Task startByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.startByName(name);
    }

    @Override
    public Task finishById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.finishById(id);
    }

    @Override
    public Task finishByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.finishByIndex(index);
    }

    @Override
    public Task finishByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.finishByName(name);
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeStatusById(id, status);
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Task changeStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeStatusByName(name, status);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return taskRepository.existsById(id);
    }

    @Override
    public boolean existsByIndex(final Integer index) {
        return taskRepository.existsByIndex(index);
    }

    @Override
    public Task findByProjectAndTaskId(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        return taskRepository.findByProjectAndTaskId(projectId, taskId);
    }


}
